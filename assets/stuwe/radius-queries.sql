SELECT groupname \
FROM ${usergroup_table} \
WHERE (username = '%{SQL-User-Name}' 
    and groupname in 
    ( select groupname from nas_vlans 
        where nas_ip = '%{request:NAS-IP-Address}'
        or nas_identifier = '%{NAS-Identifier}')
    ) \
    or username = 'default-2000' \
ORDER BY priority ASC LIMIT 1;