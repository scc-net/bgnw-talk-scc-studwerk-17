import requests

params = {'keyval_key': 'snmp_sys_descr',
          'keyval_val_regexp': '.+cisco.+',
          'netcmpnd': 'cs'
          }
url = "https://apihost/2.0/nd/device/list/"
r = requests.get(url=url, cert=('cert.pem', 'key.key'),
                 params=params)

for device in r.json():
    print(device['fqdn'])
