= Janis =
== Abstrakter Überblick KIT-Netz ==
....
== NetDB ==
-L1: Ort, parser, kabelnummern, racks,...
-L2: VLAN config
-L3: Netze, DNS, NAT, FW
+ Betreuer 
-> DNSVS, NATVS, NetDoc; API
=> Aus dieser mächtigen API leiten sich durch scripte etc. der Inahlt der u.g. softwares ab

== Vorstellung der Integrationen ==
Devices -> Netdoc
NetDoc -> Icinga 
NetDoc -> Cacti
NetDoc -> Grafana

Macfinder

=> Ausblick: Postgres, Neues DNSVS, PowerDNS, DHCP

==== Stephan ====

== Datenbank ==
 * Modell
 * Herkunft/Entstehung
	wurde gebaut, um per-zimmer-vlan Konzept realisieren zu koennen (Verweis talk markus)
	dann kam ein sonderfall... und noch einer... und ein globales vlan... 
	Werdegang von .Net zu php zu python/flask
	Gewaehltes Konzept: Abbildung des Soll-Zustandes zur config generierung
		Bsp: Zeige auf Switchseite auch aktuellen Portstatus, Counters, konfigurierte Vlans, aktuellen traffic 
 * konzept der aktuellen Entwicklung (soll zustand mit Abgleich zu ist)
 * Besonderheit viieele Vlans (3307 L3)
 	Verschiedene Vlan Gruppen
		- local (Bewohner, transfer, andere)
		- global (mgmt, eduroam, ka-wlan, ap-netz)
	Config-generierung fuer switche/router
	Automatisiertes Anlegen von vlans, zuweisung von netzen

 * D'ohs
 
== Dienste ==
 * Monitoring (libreNMS, frueher Check_MK)
 	Check_MK updates fuehrten haeufig zu Problemen (eigene Debian Pakete bauen)
	Zu wenig Metriken zu Wlan APs -> LibreNMS testweise fuer wlan installiert, nach 15min waren alle netzkomponenten automatisiert importiert.
	Einbindung zu eigner Netzdoku ueber MySQL DB & REST-API moeglich
	Auslese von metriken aus db moeglich
	Deutlich modernere UI & Graphen (Bootstrap)
 * Config Backup
 	hosts/settings aus db, aktuell noch ein shell/perl/expect/git cronjob
 * Radius dynamic vlans zuweisung
 	- Einbezug der Netzwerkdoku zur bestimmung des Vlans, welches ein User an einem spezifischen AP zugewiesen bekommt.
	- 
== Moegliche andere Loesung ==
Warum nicht ein fertiges OS IPAM/DCIM nehmen? Bsp. an Netbox
 = Vorteile =
 * Postgres DB im Hintergrund
 * Grundlegendes ist alles vorhanden
 * REST-API
 = Nachteile/Konflikte =
 * noch keine write-api
 * konzept rein auf soll-zustand 
 


== Ausblick ==
 * Wlan Controller
 * Postgres Migration
 * Zugriffsrollen
 * bewohner-interface mit eigenen v6 freischaltungen
 * Integration graylog & oxidized
